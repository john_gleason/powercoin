##Title: PowerCoin
##Version: 0.02
##Date: 6/10/2014
##Author: jagallout
##Usage: It works best to dot source the .ps1 file (e.g. PS C:\[path to PowerCoin.ps1]> . .\PowerCoin.ps1 )
##This will prompt you for a username/password and for the Uri for the rpc server, then you can just call the methods by name (e.g. getinfo)
##Question: jagallout on bitcointalk.org
##Tips: 1GqrY1LSRD4N99LQn4ULhSSoJ79B7rtC6W


$cred = Get-Credential
$Uri = Read-Host "Specify RPC Server"

$p_jsonrpc = "`"jsonrpc`":2.0"
$p_id = "`"id`":1"

$passwordFile = "~/powerCoin-info.txt"

function saveCredentials
{
    $kvp = ($cred.Username +"::::"+(ConvertFrom-SecureString $cred.Password))
    Set-Content -Path $passwordFile -Value $kvp
}

function loadCredentials
{
    if ([System.IO.File]::Exists($passwordFile))
    {
        $content = Get-Content $passwordFile
        $values = $content.Split("::::")
        $username = $values[0]
        $password = $values[1]
    
        $cred.Password = ConvertTo-SecureString $password
        $cred.Username = $username
    }
    else
    {
        $cred = Get-Credential
    }
}

function execute
{

    param
    (
    $json
    );

    $result = Invoke-WebRequest -Uri $Uri -Method Post -ContentType "application/json-rpc" -Credential $cred -Body $json

    return $result.Content
}

function getInfo
{

    $json = 
@"
{"method":"getinfo",$p_id, $p_jsonrpc}
"@

    return execute($json)

}

function getblockcount
{

    $json = 
@"
{"method":"getblockcount",$p_id, $p_jsonrpc}
"@

    return execute($json)

}

function getbestblockhash
{

    $json = 
@"
{"method":"",$p_id, $p_jsonrpc}
"@

    return execute($json)

}

function getdifficulty
{

    $json = 
@"
{"method":"getdifficulty",$p_id, $p_jsonrpc}
"@

    return execute($json) 

}

function getreceivedbyaddress 
{

    Param
    (
    [string]$address,
    [int]$confirmations
    );

    $params = "[`"$address`",$confirmations]"
    
    $json = 
@"
{"method":"getreceivedbyaddress","params":$($params.ToString()), $p_id, $p_jsonrpc}
"@

    return execute($json)
     
}

function signmessage
{
    Param
    (
    [parameter(mandatory=$true)][string]$address,
    [parameter(mandatory=$true)][string]$message
    );

    $params = "[`"$address`",`"$message`"]"

    $json = 
@"
{"method":"signmessage","params":$($params.ToString()), $p_id, $P_jsonrpc}
"@

    return execute($json)
}

function verifymessage 
{
    Param
    (
    [parameter(mandatory=$true)][string]$address,
    [parameter(mandatory=$true)][string]$signature,
    [parameter(mandatory=$true)][string]$message
    );

    $params = "[`"$address`",`"$signature`",`"$message`"]"
    
    $json = 
@"
{"method":"verifymessage","params":$($params.ToString()), $p_id, $p_jsonrpc}
"@

    return execute($json)
     
}

function getbalance 
{

    Param
    (
    [string]$account,
    [int]$confirmations
    );
   
    if (!$account) {$account = ""}
    if (!$confirmations) {$confirmations = 6}
    
    $params = "[`"$account`", $confirmations]"

    $json = 
@"
{"method":"getbalance","params":$($params.ToString()), $p_id, $p_jsonrpc}
"@

    return execute($json)

}

function getaccount
{
    Param
    (
    [string]$account
    );

    $params = "[`"$account`"]"

    $json = 
@"
{"method":"getaccount","params":$($params.ToString()), $p_id, $p_jsonrpc}
"@

    return execute($json)
}

function getaccountaddress
{
    Param
    (
    [string]$account
    );
    
    $params = "[`"$account`"]"

    $json = 
@"
{"method":"getaccountaddress","params":$($params.ToString()), $p_id, $p_jsonrpc}
"@

    return execute($json)
}

function dumpprivkey
{
    Param
    (
    [parameter(mandatory=$true)][string]$address
    );

    $params = "[`"$address`"]"

    $json =
@"
{"method":"dumpprivkey","params":$($params.ToString()), $p_id, $p_jsonrpc}
"@
    
    return execute($json)
}

function getnewaddress 
{
    Param
    (
    [string]$account
    );
    
    $params = "[`"$account`"]"

    $json =
@"
{"method":"getnewaddress","params":$($params.ToString()), $p_id, $p_jsonrpc}
"@
    return execute($json)
}

function gettransaction
{
    Param
    (
    [parameter(mandatory=$true)][string]$txid
    );

    $params = "[`"$txid`"]"

    $json =
@"
{"method":"gettransaction","params":$($params.ToString()), $p_id, $p_jsonrpc}
"@

    return execute($json)

}

function sendtoaddress #TODO: Test Functionality
{
    param
    (
        [parameter(mandatory=$true)][string]$address,
        [parameter(mandatory=$true)][decimal]$amount #api calls for "real", is decimal okay to use?
    );

    $params = "[`"$address`",$amount]"

    $json = 
@"
{"method":"sendtoaddress","params":$($params.ToString()), $p_id, $p_jsonrpc}
"@

    return execute($json)
}

function setaccount
{
    param
    (
        [parameter(mandatory=$true)][string]$address,
        [parameter(mandatory=$true)][string]$account
    );

    $params = "[`"$address`",`"$account`"]"

    $json = 
@"
{"method":"setaccount","params":$($params.ToString()), $p_id, $p_jsonrpc}
"@

    return execute($json)
}

function setgenerate
{
    param
    (
        [parameter(mandatory=$true)][bool]$enable,
        [parameter(mandatory=$false)][int]$proclimit
    );
    if ($enable) {$enableText = "true"}else{$enableText = "false"}
    if (!$proclimit){$proclimit = -1}

    $params = "[`"$enableText`",$proclimit]"

    $json = 
@"
{"method":"setgenerate","params":$($params.ToString()), $p_id, $p_jsonrpc}
"@

    return execute($json)
}

function settxfee
{
    param
    (
        [parameter(mandatory=$true)][decimal]$fee #api calls for "real", is decimal okay to use?
    );

    $params = "[`"$fee`"]"

    $json = 
@"
{"method":"settxfee","params":$($params.ToString()), $p_id, $p_jsonrpc}
"@

    return execute($json)
}

function stop
{
    $json = 
@"
{"method":"stop", $p_id, $p_jsonrpc}
"@

    return execute($json)
}

function validateaddress
{
    param
    (
        [parameter(mandatory=$true)][string]$address
    );

    $params = "[`"$address`"]"

    $json = 
@"
{"method":"validateaddress","params":$($params.ToString()), $p_id, $p_jsonrpc}
"@

    return execute($json)
}

function walletlock
{
    $json = 
@"
{"method":"walletlock", $p_id, $p_jsonrpc}
"@
    return execute($json)
}

function walletpassphrase
{
    param
    (
        [parameter(mandatory=$true)][string]$passphrase,
        [parameter(mandatory=$true)][int]$timeout
    );

    $params = "[`"$passphrase`", $timeout]"

    $json = 
@"
{"method":"walletpassphrase","params":$($params.ToString()), $p_id, $p_jsonrpc}
"@

    return execute($json)
}

function walletpassphrasechange
{
    param
    (
        [parameter(mandatory=$true)][string]$old,
        [parameter(mandatory=$true)][string]$new
    );

    $params = "[`"$old`", `"$new`"]"

    $json = 
@"
{"method":"walletpassphrasechange","params":$($params.ToString()), $p_id, $p_jsonrpc}
"@

    return execute($json)
}